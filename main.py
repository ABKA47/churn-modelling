import numpy as np
import pandas as pd
import tensorflow as tf

############# DATA PREPROCESSING ###############
################################################
# importing dataset for preprocessing

dataset = pd.read_csv('Churn_Modelling.csv')
x = dataset.iloc[:, 3:-1].values
y = dataset.iloc[:, -1].values
print(x)
print(y)

# Encoding categorical data
    # Label Encoding the Gender column
from sklearn.preprocessing import LabelEncoder

le = LabelEncoder()
x[:, 2] = le.fit_transform(x[:, 2])
print(x)

    # One Hot Encoding the Geography column
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
ct = ColumnTransformer(transformers=[('encoder', OneHotEncoder(), [1])], remainder='passthrough')
x = np.array(ct.fit_transform(x))
print(x)

#Splitting the dataset into the Trainning set and Test set
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=0)
#!IMPORTANT Fealture Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
x_train = sc.fit_transform(x_train)
x_test = sc.transform(x_test)

################## BUILDING THE ANN #################
ann = tf.keras.models.Sequential()
 #adding the input layer and the first hidden layer
ann.add(tf.keras.layers.Dense(units=6, activation='relu'))
 #adding the second hidden layer
ann.add(tf.keras.layers.Dense(units=6, activation='relu'))
 #adding the output layer
ann.add(tf.keras.layers.Dense(units=1, activation='sigmoid'))

################## TRAINING THE ANN ####################
 #Compiling the ANN
ann.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
 #Training the ANN on the Training set
ann.fit(x_train, y_train, batch_size=32, epochs=100)
################### MAKING THE PREDICTIONS AND EVALUATING THE MODEL ##########
 #Predicting the result of a single observation
print(ann.predict(sc.transform([[1, 0, 0, 600, 1, 40, 3, 60000, 2, 1, 1, 50000]])) > 5)
 ###### Predicting the Test set result #####
y_pred = ann.predict(x_test)
y_pred = (y_pred > 0.5)
print(np.concatenate((y_pred.reshape(len(y_pred), 1), y_test.reshape(len(y_test), 1)), 1))

 ###### Making the Confusion Matrix ########
from sklearn.metrics import confusion_matrix, accuracy_score
cm = confusion_matrix(y_test, y_pred)
print(cm)
accuracy_score(y_test, y_pred)